<?php

class Validation{

	public function checkAllFields($fields)
	{
		$errors = [];

		foreach ($fields as $key => $value) {
			
			if(empty($value)){
				
				$errors[$key] = $key." is Required!";
			}

		}

		return $errors;
		
	}

}

?>