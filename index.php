<?php

include 'Validation.php';

$validation = new Validation();

?>
<!DOCTYPE html>
<html>
<head>
	<title>PHP Form</title>
</head>
<body>

	<h2>PHP FORM</h2>

	<?php


		if(isset($_POST['submit'])){

			//FORM VALIDATIONS
			$result = $validation->checkAllFields($_POST);

			if( empty($result) )
			{
				echo "Passed";
			}else{

				foreach ($result as $value) {
					echo "<p style='color:red'>".$value."</p>";
				}
			}

			//FILE UPLOAD
			if(isset($_FILES['photo'])){
				
				$tmp_file = $_FILES['photo']['tmp_name'];

				$allExtensions = explode('.', $_FILES['photo']['name']);

				$extenstion = end($allExtensions);

				$fileName = uniqid().'.'.$extenstion;
				move_uploaded_file($tmp_file, "image/".$fileName);
				echo "File Uploaded";
			}

		}

	?>

	<form method="POST" action="" enctype="multipart/form-data">
		
		<input type="text" placeholder="Username" name="username" ><br><br>

		<input type="email" placeholder="Email" name="email" ><br><br>

		<input type="password" placeholder="password" name="password" ><br><br>

		<input type="file" name="photo"><br><br>

		<input type="submit" name="submit" value="Save">

	</form>

</body>
</html>